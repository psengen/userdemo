Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :user, :only => [:show, :create, :update, :verify] do
        post 'verify', to: 'user#verify'
      end
    end
  end

  namespace :api do
    namespace :v2 do
      resources :user, :only => [:show, :create, :update, :verify] do
        post 'verify', to: 'user#verify'
      end
    end
  end
end
