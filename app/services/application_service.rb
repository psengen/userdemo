class ApplicationService

  def success_response(msg, obj)
    @response = OpenStruct.new
    @response.state = 'ok'
    @response.message = msg
    @response.data =obj
    @response
  end

  def failed_response(msg,obj)
    @response = OpenStruct.new
    @response.state = 'bad_request'
    @response.message = msg
    @response.data = obj
    @response
  end

end