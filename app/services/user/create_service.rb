class User::CreateService < ApplicationService

  def initialize(params)
    @params = params
  end

  def create_user
    new_user(@params)
  end

  private
  def new_user(user_params)
    @user = User.new(user_params)
    if @user.save
      if @user.phone.present?
        success_response("User created. To verify phone, update", @user)
      else
        success_response("User created, unverified as phone is not added",@user)
      end
    else
      failed_response("User not created", @user.errors.full_messages.to_sentence)
    end

  end

end