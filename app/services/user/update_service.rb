class User::UpdateService< ApplicationService

  def initialize(params)
    @params = params
  end

  def update_user
    modify_user(@params)
  end

  private
  def modify_user(mod_params)
    response = User::SearchService.new(mod_params[:id]).set_user
    failed_response(response.message, response.data) and return if response.state != 'ok'

    @user = response.data

    if mod_params[:phone].nil? && !@user.verified
      failed_response("Phone number cannot be nil when updating", @user)
    end

    if mod_params[:phone].present?
      @user.verified=false unless @user.phone==mod_params[:phone]
    end

    if @user.verified
      if  @user.update(mod_params)
        success_response("User updated",@user)
      else
        failed_response(@user.errors.full_messages.to_sentence, @user)
      end

    else
      @user.otp = Random.rand(10000..99999) #Generate Otp
      if  @user.update(mod_params) && @user.update_attribute(:otp, @user.otp)
        success_response('User updated, verify your phone with sent otp',@user.otp)
      else
        failed_response(@user.errors.full_messages.to_sentence,@user)
      end
    end


  end
end