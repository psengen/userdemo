class User::OtpService < ApplicationService

  def initialize(params)
    @params = params
  end

  def verify_user
    check_otp(@params)

  end

  def check_otp(check_params)

    response = User::SearchService.new(check_params[:id]).set_user
    failed_response(response.message, response.data) and return if response.state != 'ok'

    @user = response.data

    if check_params[:otp]==@user.otp
      if @user.update_attribute(:verified,true)
        success_response("user verified successfully",@user)
      else
        failed_response(@user.errors.full_messages.to_sentence,@user)
      end
    else
      failed_response("User verification failed with", @user)
    end
  end

end