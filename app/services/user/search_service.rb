class User::SearchService < ApplicationService

  def initialize(param)
    @id = param
    # find_user(param)
  end

  def set_user
    find_user(@id)
  end

  private
  def find_user(id)
    @user = User.find(id)
    if @user.present?
      success_response("User found!", @user)
    else
      failed_response("User not found!", @user)
    end

  end

end