class User < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true, format: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i, uniqueness: true
  validates :phone, :numericality => true, allow_nil: true

  VISIBILITY = [:name,:email,:phone]
end
