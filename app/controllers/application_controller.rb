class ApplicationController < ActionController::Base

  skip_before_action :verify_authenticity_token

  def display(state, msg, data)

    display_message = {message: msg}
    display_message.merge!(user: data)

    render json: display_message.as_json, status: state

  end
end
