class Api::V1::UserController < ApplicationController

  skip_before_action :verify_authenticity_token
  before_action :set_user, only: [:show, :update]
  before_action :set_user_verify, only: [:verify]
  # before_action :verification_params, only: [:verify]

  def show
    render json: {id: @user.id, name: @user.name, email: @user.email, phone: @user.phone}
  end

  def create
    @user = User.new(user_params)

    if @user.save
      render json: {message: 'user created, unverified as phone is not added'},status: :ok and return if @user.phone.nil?
      render json: {message: 'user created. To verify phone, update'}
    else
      render json: { message: @user.errors.full_messages.to_sentence }
    end

  end

  def update

    if params[:phone].nil? && !@user.verified
      render json: {message: "Phone number cannot be nil when updating"}, status: :bad_request and return
    end

    if params[:phone].present?
      @user.verified=false unless @user.phone==params[:phone]
    end

    if @user.verified
       if  @user.update(user_params) && @user.update_attribute(:verified, @user.verified)
         render json: {message: "user updated"}, status: :ok
       else
         render json: {message: @user.errors.full_messages.to_sentence}
       end
    else
      generate_otp
      if  @user.update(user_params) && @user.update_attributes(:verified => @user.verified, :otp => @user.otp)
        render json: {message: 'user updated, verify your phone with sent otp', otp: @user.otp}, status: :ok
      else
        render json: {message: @user.errors.full_messages.to_sentence}
      end
    end

  end

  def verify
    if params[:otp]==@user.otp
      if @user.update_attribute(:verified,true)
        render json: {message: "user verified successfuly", verification: @user.verified}, status: :ok
      else
        render json: {message: @user.errors.full_messages.to_sentence}
      end
    else
      render json: {message: "user verification failed", otp: @user.otp, verification: @user.verified}, status: :bad_request
    end

  end

  private

  def set_user
    render json: { message: 'No user id given'}, status: :bad_request and return if params[:id].nil?
    @user = User.find(params[:id])
    render json: { message: 'No user found'}, status: :bad_request and return if @user.nil?

  end

  def set_user_verify
    render json: { message: 'No user id given'}, status: :bad_request and return if params[:id].nil?
    @user = User.find(params[:id])
    render json: { message: 'No user found', id: @user.id, otp: @user.otp}, status: :bad_request and return if @user.nil?

  end

  def user_params
    params.permit(:name, :email, :phone)
  end

  def verification_params
    params.permit(:id, :otp)
  end

  def generate_otp
    @user.otp = Random.rand(10000..99999)
  end

end


