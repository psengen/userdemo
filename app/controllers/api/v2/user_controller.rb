class Api::V2::UserController < ApplicationController

  def show
    response = User::SearchService.new(params[:id]).set_user
    render json: {status: response.state, message: response.message, data: response.data.as_json(only: User::VISIBILITY)}
    # display(response.state,response.message,response.data)
  end

  def create
    response = User::CreateService.new(user_params).create_user
    render json: {status: response.state, message: response.message, data: response.data.as_json(only: User::VISIBILITY)}
    # display(response.state,response.message,response.data)
  end

  def update
    response = User::UpdateService.new(update_params).update_user
    render json: {status: response.state, message: response.message, data: response.data.as_json(only: User::VISIBILITY)}
    # display(response.state,response.message,response.data)
  end

  def verify
    response = User::OtpService.new(verification_params).verify_user
    render json: {status: response.state, message: response.message, data: response.data.as_json(only: User::VISIBILITY)}
  end

  private
  def user_params
    params.permit(:name, :email, :phone)
  end

  def update_params
    params.permit(:id, :name, :email, :phone)
  end

  def verification_params
    params.permit(:id, :otp)
  end

end